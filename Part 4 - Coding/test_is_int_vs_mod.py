from timeit import Timer

def using_is_interger():
    for i in range(1, 100):
        if (i/3).is_integer() and (i/5).is_integer():
            a1.append(i)
        else:
            if (i/3).is_integer():
                b1.append(i)
            if (i/5).is_integer():
                c1.append(i)
            else:
                d1.append(i)

def using_mod():
    for i in range(1, 100):
        if i%3 == 0 and i%5 == 0:
            a2.append(i)
        else:
            if i%3 == 0:
                b2.append(i)
            if i%5 == 0:
                c2.append(i)
            else:
                d2.append(i)


a1 = b1 = c1 = d1 = a2 = b2 = c2 = d2 = []
x = Timer(lambda: using_is_interger()).timeit(number=10000)
y = Timer(lambda: using_mod()).timeit(number=10000)

if a1 == a2:
    print("List A are Equales")

if b1 == b2:
    print("List B are Equales")

if c1 == c2:
    print("List C are Equales")

if d1 == d2:
    print("List D are Equales")

if x > y:
    print('using_mod is faster than .is_interger', round(x,2), round(y,2))
else:
    print('is_interger is faster than using_mod', round(x,2), round(y,2))
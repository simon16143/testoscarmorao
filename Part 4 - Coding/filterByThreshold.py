"""
Write a function filterByThreshold which takes in three parameters:
1. myArr - An array of integers
2. threshold - An integer
3. greaterThan - A boolean value. True means greater than the threshold
and false means less than or equal to the threshold
Return and print an array containing all integers from myArr which are above or
below the threshold based on the values of parameter 2 and 3.
Sample:
filterByThreshold([1,3,6,8, 10], 5, false)
Output: [1,3]
Explanation:
1 < 5 --> true and the boolean greaterThan is set to false so we are looking for elements that
are less than or equal to the threshold. The result so far would be [1].
3 < 5 --> true and the boolean greaterThan is set to false. So we are looking for elements that
are less than or equal to the threshold. The result so far would be [1,5].
6 < 5 --> false and the boolean greaterThan is set to false. So we are looking for elements that
are less than or equal to the threshold. This does not change our result.
etc.
"""

# Function Definition
def filterByThreshold(myArr,threshold,greatherThan):
    greatherThan = list(map(lambda x: True if x <= threshold else False, myArr))
    filtered = [i for i, j in zip(myArr, greatherThan) if j == True]
    print(f'Received Values:\n', myArr,'\n')
    print(f'Threshold: ', threshold,'\n')
    print(f'All Booleans:\n', greatherThan,'\n')
    print(f'Filtered List:\n', filtered,'\n')
        
# Calling Funtion
filterByThreshold([1,3,6,8,10],5,False)


# testOscarMorao

## Part 1: SQL
Please write a query in SQL that will return a list of unique products that were bought on
the
retailer’s site since December 3rd with most expensive product returned first.

### Answer:
#### Short Answer:

    SELECT Product.productID, Product.productname, Product.productprice, Purchased.creationdate
    FROM Product
    INNER JOIN Purchased
    ON Product.productid=Purchased.productID
    WHERE Purchased.creationdate > '2020-12-03 00:00:00'
    ORDER BY productprice DESC

#### Long Answer:

    Run python file SQL.py in folder PART 1 - SQL


## Part 2: CDB
    Answer in folder PART 2 - CDB.pdf


## Part 3: RTB
    Answer in folder PART 3 - RTB.pdf

    RUN python script testingURLs.py  (It should also print the ad)
OR:

    RUN ./curlRequest.json

Documentation:
https://www.iab.com/wp-content/uploads/2016/03/OpenRTB-API-Specification-Version-2-5-FINAL.pdf

## Part 4: Coding
    Answer in folder PART 4 - filterByThreshold.py

## Part 5: Inapp
    Answer in folder PART 5 - Inapp.pdf

## Part 6: Business communication

Commercial team got an email from a publisher interested in implementing CDB solution
on their webpage. Potential client is worried to place Criteo code on the page. They have
concerns about viruses and extra added load. Please write a short email response re-
assuring the publisher that implementation is safe and reliable.

### Answer:
    Hi dear Customer,

    I hope you are well, I would like to invite you to take a look to our success stories, where you can see that most than 20.000 companies trust in Criteo and are already using our services.
    https://www.criteo.com/success-stories/

    Also, please take a look to this link, you will find all the partners we have around the world to have a better performance every day.
    https://www.criteo.com/privacy/our-partners/

    Why CDB is secure and faster?
    It was created to be fast, lightweight and easy to use. also, all the comunication is based on HTTP's and TSL/SSL encrypted data with strong algorithms. There is no way to hack the packages we manage and we invest in keep upgrading our backend according to the Common Vulnerabilities and Exposures records. https://cve.mitre.org/

    Benefits of CDB?
    The digital ads market expect to gain more than 300US Billions until 2022, to grow up in the digital market is needed to use the lastest strategies. You can eliminate third party feeds and get more benefits using Criteo CDB and while More demand, more revenue.

    Any further doubt you can ask us and we'll be ready to help.

    I'll be looking forward to your reply.

    Regards.

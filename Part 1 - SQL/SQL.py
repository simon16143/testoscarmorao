"""Part 1: SQL
A database CriteoDB contains three tables: Product, User and Purchased.

Product contains:
    • A unique identifier (productID)
    • A name (productname)
    • A price (productprice)
User contains:
    • A unique identifier (userID)
Purchased contains:
    • Two identifiers noted as private keys, productID anduserID
    • A date of the record (creationdate)

Question:
1. Please write a query in SQL that will return a list of unique products that were bought on
the retailer’s site since December 3rd with most expensive product returned first.
"""


import os
import datetime
import sqlite3
import string
import random as R
from sqlite3 import Error

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.abspath(__file__))

db_name = 'CriteoDB'
randomVariables = 50

class db():
    def create_connection(db_file):
        #Create a database connection to a SQLite database
        conn = None
        try:
            if not os.path.exists(db_file):
                print("Database doesn't exist. Creating new database...")
            else:
                print("Erasing current Database...")
                os.remove(db_file)
            conn = sqlite3.connect(db_file)
            # conn.execute("PRAGMA foreign_keys = ON")
            # conn.execute("PRAGMA foreign_keys = 1")
        except Error as e:
            print(e)
        return conn

    def create_table(conn):
        sql1 = """CREATE TABLE IF NOT EXISTS Product (
                                    productID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                                    productname TEXT,
                                    productprice FLOAT
                                ); """       

        sql2 = """CREATE TABLE IF NOT EXISTS User (
                                    userID INTEGER NOT NULL PRIMARY KEY
                                ); """      

        sql3 = """CREATE TABLE IF NOT EXISTS Purchased (
                                    purchasedID INTEGER NOT NULL PRIMARY KEY,
                                    creationdate TIMESTAMP,
                                    productID INTERGER,
                                    userID INTERGER,
                                    FOREIGN KEY(productID) REFERENCES Product(productID)
                                    FOREIGN KEY(userID) REFERENCES User(userID)
                                ); """                                  
        try:
            c = conn.cursor()
            c.execute(sql1)
            c.execute(sql2)
            c.execute(sql3)
            conn.commit()
        except Error as e:
            print(e)

    def add_values(conn):
        #Adding values to Product Table
        for i in range(randomVariables):
            print(f"Adding values to Product Table {int(i/randomVariables*100)}%")
            letters = string.ascii_lowercase
            name = ''.join(R.choice(letters) for i in range(10))
            price = R.randrange(0,1000)
            sql = f"""INSERT INTO Product(productname,productprice)
                    VALUES('{name}',{price})"""
            cur = conn.cursor()
            cur.execute(sql)
            conn.commit()

        #Adding values to User Table
        for i in range(randomVariables):
            print(f"Adding values to User Table {int(i/randomVariables*100)}%")
            cur = conn.cursor()
            cur.execute(f"""INSERT INTO User(userID) VALUES({i+1})""")
            conn.commit()

        #Working in Purchase Table
        #Function to Create random date
        startDate = datetime.datetime(2015, 9, 20,13,00)
        dataList = []
        def random_date(start,end):
            current = start
            while end >= 0:
                curr = current + datetime.timedelta(weeks=R.randrange(300),days=R.randrange(30), hours=R.randrange(60), minutes=R.randrange(60))
                yield curr
                end-=1  
        for i in random_date(startDate,randomVariables):
            dataList.append(i)

        #Adding values to Purchased Table
        for i in range(randomVariables):
            print(f"Adding values to Purchased Table {int(i/randomVariables*100)}%")
            sql = f"""INSERT INTO Purchased(creationdate, productID, userID)
                    VALUES('{dataList[i]}',
                    (SELECT productID from Product WHERE productID = {i+1}),
                    (SELECT userID from User WHERE userID = {i+1})
                    )"""
            cur = conn.cursor()
            cur.execute(sql)
            conn.commit()
        
        #Query to solve the question.
        sql = """
                SELECT Product.productID, Product.productname, Product.productprice, Purchased.creationdate
                FROM Product
                INNER JOIN Purchased
                ON Product.productid=Purchased.productID
                WHERE Purchased.creationdate > '2020-12-03 00:00:00'
                ORDER BY productprice DESC
                """
        print(f"SQL Query {sql}")
        cur = conn.cursor()
        cur.execute(sql)
        conn.commit()
        result = []
        for i in cur:
            result.append(i)
        if result:
            print("ID  |    Name   | Price  |   Date")
            for i in result:
                print(i)
        else:
            print("There is no registers in the selected query")


# Connecting to database or creating if case it doesn't exists
conn = db.create_connection(os.path.join(BASE_DIR, db_name))

# if there is a database connection:
if conn is not None:
    db.create_table(conn)
    db.add_values(conn)

